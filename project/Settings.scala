import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import sbt.Keys._
import sbt._
import scoverage.ScoverageKeys.{coverageFailOnMinimum, coverageMinimum}

object Settings {

  implicit class ProjectFrom(project: Project) {

    def defaultSettings: Project =
      project
        .settings(
          Defaults.itSettings,
          organization := "com.foo.bar",
          scalaVersion := "2.12.8",
          version := buildVersion.value,
          coverageMinimum := sys.env.getOrElse("COVERAGE_MINIMUM", "75.0").toDouble,
          coverageFailOnMinimum := sys.env.getOrElse("COVERAGE_FAILURE", "true").toBoolean,
          parallelExecution in Test := false
        )
        .settings(dockerSettings)

    lazy val dockerSettings: Seq[Def.Setting[_]] = Seq(
      dockerUpdateLatest := true
    )

    lazy val buildVersion = Def.setting(sys.env.getOrElse("DOCKER_TAG", "local"))

  }

}
