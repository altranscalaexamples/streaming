import sbt._

object Dependencies {

  private[this] val sparkVersion = "2.4.3"
  private val spark = Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion,
    "org.apache.spark" %% "spark-sql"  % sparkVersion
  )

  private[this] lazy val akkaVersion     = "2.5.21"
  private[this] lazy val akkaHttpVersion = "10.1.7"
  private[this] lazy val alpakkaVersion  = "1.0.5"
  private lazy val akkaDependencies = Seq(
    "com.typesafe.akka"          %% "akka-http"                     % akkaHttpVersion,
    "com.typesafe.akka"          %% "akka-http-spray-json"          % akkaHttpVersion,
    "com.typesafe.akka"          %% "akka-stream"                   % akkaVersion,
    "com.typesafe.scala-logging" %% "scala-logging"                 % "3.9.0",
    "com.typesafe.akka"          %% "akka-stream-kafka"             % alpakkaVersion,
    "com.lightbend.akka"         %% "akka-stream-alpakka-cassandra" % "1.1.2"
  )

  private lazy val kafkaDependencies = Seq(
    "org.apache.kafka" % "kafka-clients"        % "2.0.0",
    "org.apache.kafka" %% "kafka-streams-scala" % "2.1.0",
    "javax.ws.rs"      % "javax.ws.rs-api"      % "2.1.1" artifacts Artifact("javax.ws.rs-api", "jar", "jar")
  )

  private[this] lazy val slf4jVersion = "1.7.25"
  private lazy val logDependencies = Seq(
    "org.slf4j" % "slf4j-api"     % slf4jVersion,
    "org.slf4j" % "slf4j-log4j12" % slf4jVersion,
    "log4j"     % "log4j"         % "1.2.17"
  )

  private[this] lazy val versionScalaTest = "3.0.5"
  private lazy val testDependencies = Seq(
    "org.apache.kafka"        % "kafka-streams-test-utils"   % "2.3.0"          % Test,
    "io.github.embeddedkafka" %% "embedded-kafka"            % "2.3.1"          % "it, test",
    "com.typesafe.akka"       %% "akka-testkit"              % akkaVersion      % "it, test",
    "com.typesafe.akka"       %% "akka-http-testkit"         % akkaHttpVersion  % "it, test",
    "com.typesafe.akka"       %% "akka-stream-testkit"       % akkaVersion      % "it, test",
    "com.typesafe.akka"       %% "akka-stream-kafka-testkit" % alpakkaVersion   % "it, test",
    "org.scalatest"           %% "scalatest"                 % versionScalaTest % "it, test",
    "org.mockito"             % "mockito-all"                % "1.10.19"        % "it, test"
  )

  val dependencies: Seq[ModuleID] =
    akkaDependencies
      .++(kafkaDependencies)
      .++(logDependencies)
      .++(testDependencies)

}
