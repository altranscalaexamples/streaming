import sbt.Keys._
import sbt._
import Settings._
import Dependencies._

scalacOptions += "-Ypartial-unification"
scalacOptions += "-language:higherKinds"

lazy val streaming = (project in file("."))
  .configs(IntegrationTest)
  .settings(name := "streaming")
  .settings(libraryDependencies ++= dependencies)
  .defaultSettings
  .enablePlugins(JavaAppPackaging, DockerPlugin, AshScriptPlugin)

fork in run := true

mainClass in Compile := (mainClass in Compile in streaming).value

inConfig(IntegrationTest)(org.scalafmt.sbt.ScalafmtPlugin.scalafmtConfigSettings)
