package com.foo.bar

import com.typesafe.config.ConfigFactory

object Config {

  private val config = ConfigFactory.load()

  val bootStrapServer = config.getString("kafka.bootstrapServers")
  val contactPoints   = config.getString("cassandra.contactPoints")
  val port            = config.getString("cassandra.port")

  val customerTopic      = "customer"
  val orderTopic         = "order"
  val invertedOrderTopic = "inverted_order"
  val joinTopic          = "customer_orders"

  val cassandraKs = config.getString("cassandra.keyspace")

}
