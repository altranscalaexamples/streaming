package com.foo.bar

import java.time.Duration

import akka.actor.{ActorSystem, Scheduler}
import akka.stream.ActorMaterializer
import com.datastax.driver.core.{BoundStatement, Cluster, PreparedStatement, Session}
import com.foo.bar.streams.cassandra.{CassandraSink, CustomerSink}
import com.foo.bar.streams.flow.StreamCassandraFlow
import com.foo.bar.model.Customer
import com.foo.bar.streams.job.CopyOrderJob
import com.foo.bar.streams.kafka.{Consumer, KafkaConsumer, RequestSource}

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

object Main extends StreamCassandraFlow {

  import com.foo.bar.model.JsonFormat._

  implicit val system: ActorSystem                = ActorSystem("system")
  implicit val materializer: ActorMaterializer    = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher
  implicit val scheduler: Scheduler               = system.getScheduler

  def getCustomerCassandraSink(cluster: Cluster, keyspace: String, tableName: String): CassandraSink[Customer] =
    CustomerSink(cluster, keyspace, tableName)

  implicit val consumer: Consumer = new KafkaConsumer(Config.bootStrapServer)

  def main(args: Array[String]): Unit = {
    waitUntilCassandraIsUp

    val cluster = Cluster
      .builder()
      .addContactPoint(Config.contactPoints)
      .withPort(Config.port.toInt)
      .build()

    val source = new RequestSource[Customer]()
    val sink   = getCustomerCassandraSink(cluster, Config.cassandraKs, Config.customerTopic)

    val flow = requestFlow(Config.customerTopic, source, sink)

    val job = CopyOrderJob.stream(Config.orderTopic, Config.invertedOrderTopic)

    job.start()

    sys.addShutdownHook {

      flow.onComplete {
        case Success(value)     => println(value)
        case Failure(exception) => throw exception
      }

      job.close(Duration.ofSeconds(15L))

      sink.session.closeAsync()
      cluster.closeAsync()
    }
  }

  @tailrec
  def waitUntilCassandraIsUp: Unit = {
    val cluster = Cluster
      .builder()
      .addContactPoint(Config.contactPoints)
      .withPort(Config.port.toInt)
      .build()
    Try(cluster.connect()) match {
      case Success(s) => s.close()
      case Failure(exception) =>
        logger.warn(exception.getMessage)
        Thread.sleep(1000)
        waitUntilCassandraIsUp
    }
  }
}
