package com.foo.bar.streams.job

import java.util.Properties

import com.foo.bar.Config
import com.foo.bar.model.Order
import org.apache.kafka.streams.scala.kstream.KStream
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import org.apache.kafka.streams.scala.StreamsBuilder
import com.foo.bar.streams.serialization._

class CopyOrderJob(orderInputTopic: String, orderOutputTopic: String) {

  val streamProps = new Properties()
  streamProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "foo")
  streamProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, Config.bootStrapServer)
  streamProps.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0")
  streamProps.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE)

  def createTopology(): StreamsBuilder = {

    val builder = new StreamsBuilder()

    val orderInput: KStream[String, Order] = builder.stream[String, Order](orderInputTopic)

    val invertedOrder: KStream[String, Order] = orderInput.selectKey { case (_, value) => value.customerId }

    invertedOrder.to(orderOutputTopic)

    builder
  }

  def createStream(streamsBuilder: StreamsBuilder) = new KafkaStreams(streamsBuilder.build(), streamProps)

}

object CopyOrderJob {

  def stream(orderInputTopic: String, orderOutputTopic: String): KafkaStreams = {
    val job      = new CopyOrderJob(orderInputTopic, orderOutputTopic)
    val topology = job.createTopology()
    job.createStream(topology)
  }

}
