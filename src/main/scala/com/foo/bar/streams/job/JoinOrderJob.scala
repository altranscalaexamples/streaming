package com.foo.bar.streams.job

import java.util.Properties

import com.foo.bar.Config
import com.foo.bar.model.{Customer, CustomerOrder, Order}
import com.foo.bar.streams.serialization._
import org.apache.kafka.streams.scala.kstream.{KStream, KTable, Materialized}
import org.apache.kafka.streams.scala.{ByteArrayKeyValueStore, StreamsBuilder}
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import KafkaSerdes._

class JoinOrderJob(orderInputTopic: String, customerInputTopic: String, joinTopic: String) {

  val orderMat: Materialized[String, Order, ByteArrayKeyValueStore] = Materialized.as[String, Order, ByteArrayKeyValueStore]("order_store_2")
  val customerMat: Materialized[String, Customer, ByteArrayKeyValueStore] =
    Materialized.as[String, Customer, ByteArrayKeyValueStore]("customer_store_2")

  val streamProps = new Properties()
  streamProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "foo")
  streamProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, Config.bootStrapServer)
  streamProps.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0")
  streamProps.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE)

  def createTopology(): StreamsBuilder = {
    val builder = new StreamsBuilder()

    val orderInput = builder
      .stream[String, Order](orderInputTopic)
      .groupByKey(serilalizedOrder)
      .reduce((_, newValue) => newValue)(orderMat)

    val customerInput: KTable[String, Customer] = builder
      .stream[String, Customer](customerInputTopic)
      .groupByKey(serilalizedCustomer)
      .reduce((_, newValue) => newValue)(customerMat)

    val joined: KStream[String, CustomerOrder] = customerInput
      .join(orderInput)(joiner)
      .toStream

    joined
      .flatMapValues(v => Option(v))
      .selectKey((_, v) => v.orderId)
      .to(joinTopic)

    builder
  }

  def joiner: (Customer, Order) => CustomerOrder =
    (customer, order) => CustomerOrder(customer.customerId, customer.name, order.orderId, order.price)

  def createStream(streamsBuilder: StreamsBuilder) = new KafkaStreams(streamsBuilder.build(), streamProps)

}

object JoinOrderJob {

  def stream(orderInputTopic: String, customerInputTopic: String, joinTopic: String): KafkaStreams = {
    val job      = new JoinOrderJob(orderInputTopic, customerInputTopic, joinTopic)
    val topology = job.createTopology()
    job.createStream(topology)
  }

}
