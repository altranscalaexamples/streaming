package com.foo.bar.streams.kafka

import akka.actor.ActorSystem
import akka.kafka.{ConsumerMessage, Subscriptions}
import akka.stream.scaladsl.Source
import com.foo.bar.streams.kafka.config.CustomConsumerSettings

class KafkaConsumer(val kafkaServer: String) extends Consumer with CustomConsumerSettings {

  def consumeStream(
      topic: String
  )(implicit system: ActorSystem): Source[ConsumerMessage.CommittableMessage[String, String], akka.kafka.scaladsl.Consumer.Control] =
    akka.kafka.scaladsl.Consumer.committableSource[String, String](consumerSettings, Subscriptions.topics(topic))

  override val kafkaBootstrapServers: String = kafkaServer

}
