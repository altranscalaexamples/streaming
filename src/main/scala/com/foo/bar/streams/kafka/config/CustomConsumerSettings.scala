package com.foo.bar.streams.kafka.config

import akka.actor.ActorSystem
import akka.kafka.ConsumerSettings
import org.apache.kafka.common.serialization.StringDeserializer

trait CustomConsumerSettings {

  val groupId: String = "test-group"
  val kafkaBootstrapServers: String

  def consumerSettings(implicit actorSystem: ActorSystem): ConsumerSettings[String, String] = {
    val config = actorSystem.settings.config.getConfig("akka.kafka.consumer")
    ConsumerSettings(config, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers(kafkaBootstrapServers)
      .withGroupId(groupId)
  }
}
