package com.foo.bar.streams.kafka

import spray.json.RootJsonFormat

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import spray.json._

class RequestSource[T](
    implicit ec: ExecutionContext,
    formatConf: RootJsonFormat[T]
) extends RecordConsumerService[T] {

  override def consumeInputRecord(
      inputRecord: String
  ): Either[Throwable, T] =
    Try(inputRecord.parseJson.convertTo[T]).toEither

}
