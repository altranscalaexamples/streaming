package com.foo.bar.streams.kafka

import akka.kafka.ConsumerMessage.CommittableMessage
import akka.stream.scaladsl.Flow

import scala.concurrent.{ExecutionContext}

trait RecordConsumerService[T] {

  def consumeInputRecord(
      inputRecord: String
  ): Either[Throwable, T]

  def consumeMessage(implicit ec: ExecutionContext) =
    Flow[CommittableMessage[String, String]]
      .map(msg => (msg.record.key(), consumeInputRecord(msg.record.value())))

}
