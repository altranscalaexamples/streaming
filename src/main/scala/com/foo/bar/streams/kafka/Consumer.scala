package com.foo.bar.streams.kafka

import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage
import akka.kafka.scaladsl.Consumer.Control
import akka.stream.scaladsl.Source

trait Consumer {
  def consumeStream(topic: String)(implicit system: ActorSystem): Source[ConsumerMessage.CommittableMessage[String, String], Control]
}
