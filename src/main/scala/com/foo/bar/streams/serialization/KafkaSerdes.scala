package com.foo.bar.streams.serialization

import com.foo.bar.model.{Customer, CustomerOrder, Order}
import org.apache.kafka.common.serialization.Serde

object KafkaSerdes {

  import com.foo.bar.model.JsonFormat._

  implicit val stringSerde: Serde[String]               = SerdeFactory[String]
  implicit val customerSerde: Serde[Customer]           = SerdeFactory[Customer]
  implicit val orderSerde: Serde[Order]                 = SerdeFactory[Order]
  implicit val customerOrderSerde: Serde[CustomerOrder] = SerdeFactory[CustomerOrder]

}
