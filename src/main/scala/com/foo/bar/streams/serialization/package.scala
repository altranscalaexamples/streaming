package com.foo.bar.streams

import com.foo.bar.model.{Customer, CustomerOrder, Order}
import org.apache.kafka.streams.kstream.{Consumed, Grouped, Produced}
import org.apache.kafka.streams.scala.Serdes

package object serialization {

  implicit val consumedCustomer: Consumed[String, Customer]             = Consumed.`with`(Serdes.String, KafkaSerdes.customerSerde)
  implicit val consumedOrder: Consumed[String, Order]                   = Consumed.`with`(Serdes.String, KafkaSerdes.orderSerde)
  implicit val serilalizedCustomer: Grouped[String, Customer]           = Grouped.`with`(Serdes.String, KafkaSerdes.customerSerde)
  implicit val serilalizedOrder: Grouped[String, Order]                 = Grouped.`with`(Serdes.String, KafkaSerdes.orderSerde)
  implicit val serilalizedCustomerOrder: Grouped[String, CustomerOrder] = Grouped.`with`(Serdes.String, KafkaSerdes.customerOrderSerde)
  implicit val producedCustomer: Produced[String, Customer]             = Produced.`with`(Serdes.String, KafkaSerdes.customerSerde)
  implicit val producedOrder: Produced[String, Order]                   = Produced.`with`(Serdes.String, KafkaSerdes.orderSerde)
  implicit val producedCustomerOrder: Produced[String, CustomerOrder]   = Produced.`with`(Serdes.String, KafkaSerdes.customerOrderSerde)
}
