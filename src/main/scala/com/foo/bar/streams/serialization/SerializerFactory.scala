package com.foo.bar.streams.serialization

import java.util

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.apache.kafka.common.serialization.Serializer

object SerializerFactory {

  import spray.json._

  def apply[T](implicit format: JsonFormat[T]): Serializer[T] with DefaultJsonProtocol with SprayJsonSupport =
    new Serializer[T] with DefaultJsonProtocol with SprayJsonSupport {

      def serialize(topic: String, data: T): Array[Byte] =
        data.toJson.toString().getBytes

      override def close(): Unit = ()

      override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()
    }

}
