package com.foo.bar.streams.serialization

import java.util

import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}
import spray.json.JsonFormat

object SerdeFactory {

  def apply[T](implicit format: JsonFormat[T]): Serde[T] = new Serde[T] {
    def deserializer(): Deserializer[T]                                        = DeserializerFactory[T]
    def serializer(): Serializer[T]                                            = SerializerFactory[T]
    override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = ()
    override def close(): Unit                                                 = ()
  }

}
