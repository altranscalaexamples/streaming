package com.foo.bar.streams.flow

import akka.Done
import akka.actor.ActorSystem
import akka.kafka.CommitterSettings
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.stream.scaladsl.{Flow, Keep}
import akka.stream.{ActorAttributes, Materializer, Supervision}
import com.datastax.driver.core.Session
import com.foo.bar.streams.cassandra.CassandraSink
import com.foo.bar.streams.kafka.{Consumer, RecordConsumerService}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.{ExecutionContext, Future}

trait StreamCassandraFlow extends StrictLogging {

  implicit val system: ActorSystem
  implicit val materializer: Materializer
  implicit val executionContext: ExecutionContext

  lazy val committerSettings: CommitterSettings = CommitterSettings(system)

  val decider: Supervision.Decider = {
    case e: Throwable => {
      logger.error(s"Error in StreamRequestFlow: ${e.getMessage} Resuming.", e)
      Supervision.Resume
    }
  }

  def requestFlow[T](
      inTopic: String,
      source: RecordConsumerService[T],
      sink: CassandraSink[T]
  )(implicit consumer: Consumer): Future[Done] =
    consumer
      .consumeStream(inTopic)
      .via(source.consumeMessage)
      .via(Flow[(String, Either[Throwable, T])].map {
        case (_, value) =>
          value match {
            case Right(v) =>
              logger.info(s"received $v")
              v
            case Left(ex) => throw ex
          }
      })
      .withAttributes(ActorAttributes.supervisionStrategy(decider))
      .runWith(sink.sink)

}
