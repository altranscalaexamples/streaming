package com.foo.bar.streams.cassandra

import akka.Done
import akka.stream.scaladsl.Sink
import com.datastax.driver.core.{BoundStatement, PreparedStatement, Session}

import scala.concurrent.Future

trait CassandraSink[T] {

  val keyspace: String

  implicit val session: Session

  val preparedStatement: PreparedStatement

  def statementBinder: (T, PreparedStatement) => BoundStatement

  def sink: Sink[T, Future[Done]] =
    akka.stream.alpakka.cassandra.scaladsl.CassandraSink[T](parallelism = 2, preparedStatement, statementBinder)

}
