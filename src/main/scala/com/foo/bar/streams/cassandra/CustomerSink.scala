package com.foo.bar.streams.cassandra

import com.datastax.driver.core.{BoundStatement, Cluster, PreparedStatement, Session}
import com.foo.bar.model.Customer

class CustomerSink(cluster: Cluster, override val keyspace: String, tableName: String) extends CassandraSink[Customer] with Init {

  override lazy implicit val session: Session = cluster.connect()

  override lazy val preparedStatement: PreparedStatement = session.prepare(s"INSERT INTO $keyspace.$tableName (customerId, name) VALUES (?, ?) ")

  override def statementBinder: (Customer, PreparedStatement) => BoundStatement = (customer, stm) => stm.bind(customer.customerId, customer.name)

  override val createKeyspaceStm: String =
    s"CREATE KEYSPACE IF NOT EXISTS $keyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1 }"

  override val createTableStm: String = s"CREATE TABLE IF NOT EXISTS $keyspace.$tableName (customerId text PRIMARY KEY, name text)"
}

object CustomerSink {
  def apply(cluster: Cluster, keyspace: String, tableName: String): CassandraSink[Customer] = {
    val cassandraSink = new CustomerSink(cluster, keyspace, tableName)
    cassandraSink.init
    cassandraSink
  }
}
