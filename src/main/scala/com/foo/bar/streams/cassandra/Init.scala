package com.foo.bar.streams.cassandra

import com.datastax.driver.core.Session
import com.typesafe.scalalogging.LazyLogging

trait Init extends LazyLogging {

  implicit val session: Session
  val createTableStm: String
  val createKeyspaceStm: String

  def init = {
    logger.info(s"Executing $createKeyspaceStm")
    session.execute(createKeyspaceStm)
    logger.info(s"Executing $createTableStm")
    session.execute(createTableStm)
  }

}
