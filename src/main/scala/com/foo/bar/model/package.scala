package com.foo.bar

package object model {

  case class Customer(customerId: String, name: String)
  case class Order(orderId: String, customerId: String, price: Int)
  case class CustomerOrder(customerId: String, name: String, orderId: String, price: Int)
}
