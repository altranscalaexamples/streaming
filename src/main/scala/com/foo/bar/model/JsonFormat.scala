package com.foo.bar.model

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

object JsonFormat extends DefaultJsonProtocol {

  implicit val orderFormat: RootJsonFormat[Order]                 = jsonFormat3(Order.apply)
  implicit val customerFormat: RootJsonFormat[Customer]           = jsonFormat2(Customer.apply)
  implicit val customerOrderFormat: RootJsonFormat[CustomerOrder] = jsonFormat4(CustomerOrder.apply)
}
