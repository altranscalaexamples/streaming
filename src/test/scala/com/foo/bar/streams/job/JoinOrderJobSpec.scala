package com.foo.bar.streams.job

import java.util.Properties

import com.foo.bar.model.{Customer, CustomerOrder, Order}
import com.foo.bar.streams.serialization.KafkaSerdes
import org.apache.kafka.common.serialization.{Serdes, StringDeserializer, StringSerializer}
import org.apache.kafka.streams.state.KeyValueStore
import org.apache.kafka.streams.test.{ConsumerRecordFactory, OutputVerifier}
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig, TopologyTestDriver}
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}

class JoinOrderJobSpec extends WordSpec with Matchers with BeforeAndAfterEach {

  val props = new Properties()
  props.put(StreamsConfig.APPLICATION_ID_CONFIG, "foo")
  props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE)
  props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, classOf[Serdes.StringSerde])

  val builder         = new JoinOrderJob("orderIn", "customerIn", "join").createTopology()
  val factoryOrder    = new ConsumerRecordFactory[String, Order](new StringSerializer(), KafkaSerdes.orderSerde.serializer())
  val factoryCustomer = new ConsumerRecordFactory[String, Customer](new StringSerializer(), KafkaSerdes.customerSerde.serializer())

  val testDriver = new TopologyTestDriver(builder.build(), props)

  val storeOrd: KeyValueStore[String, CustomerOrder]  = testDriver.getKeyValueStore[String, CustomerOrder]("order_store_2")
  val storeCust: KeyValueStore[String, CustomerOrder] = testDriver.getKeyValueStore[String, CustomerOrder]("customer_store_2")

  override def beforeEach(): Unit = {
    storeOrd.all().forEachRemaining(keyValue => storeOrd.delete(keyValue.key))
    storeCust.all().forEachRemaining(keyValue => storeCust.delete(keyValue.key))
  }

  "JoinOrderJob" when {

    "is created" should {

      "start with no errors" in {
        val stream: KafkaStreams = JoinOrderJob.stream("orderIn", "customerIn", "join")
        Thread.sleep(2000)
        stream.close()
      }
    }

    "is running" should {

      "populate join topic" in {

        val orderId  = "order_id"
        val clientId = "client_id"
        val order    = Order(orderId, clientId, 3)
        val customer = Customer(clientId, "Mario")

        testDriver.pipeInput(factoryCustomer.create("customerIn", clientId, customer))
        testDriver.pipeInput(factoryOrder.create("orderIn", clientId, order))

        val result = testDriver.readOutput("join", new StringDeserializer, KafkaSerdes.customerOrderSerde.deserializer())

        val output = CustomerOrder(clientId, customer.name, order.orderId, order.price)
        OutputVerifier.compareKeyValue(result, orderId, output)

      }

      "populate join topic and aggregate orders" in {

        val orderId  = "order_id"
        val orderId2 = "order_id2"
        val clientId = "client_id"
        val order    = Order(orderId, clientId, 3)
        val order2   = Order(orderId2, clientId, 3)
        val customer = Customer(clientId, "Mario")

        testDriver.pipeInput(factoryCustomer.create("customerIn", clientId, customer))
        testDriver.pipeInput(factoryOrder.create("orderIn", clientId, order))

        val result = testDriver.readOutput("join", new StringDeserializer, KafkaSerdes.customerOrderSerde.deserializer())

        testDriver.pipeInput(factoryOrder.create("orderIn", clientId, order2))
        val result2 = testDriver.readOutput("join", new StringDeserializer, KafkaSerdes.customerOrderSerde.deserializer())

        val output  = CustomerOrder(clientId, customer.name, order.orderId, order.price)
        val output2 = CustomerOrder(clientId, customer.name, order2.orderId, order2.price)

        OutputVerifier.compareKeyValue(result, orderId, output)
        OutputVerifier.compareKeyValue(result2, orderId2, output2)
      }

    }
  }
}
