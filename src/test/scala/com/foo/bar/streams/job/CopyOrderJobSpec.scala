package com.foo.bar.streams.job

import java.util.Properties

import com.foo.bar.model.Order
import com.foo.bar.streams.serialization.{KafkaSerdes, SerdeFactory}
import org.apache.kafka.common.serialization.{Serdes, StringDeserializer, StringSerializer}
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpec}
import org.apache.kafka.streams.test.{ConsumerRecordFactory, OutputVerifier}
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig, TopologyTestDriver}

class CopyOrderJobSpec extends WordSpec with Matchers with BeforeAndAfterEach {

  val props = new Properties()
  props.put(StreamsConfig.APPLICATION_ID_CONFIG, "foo")
  props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE)
  props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, classOf[Serdes.StringSerde])

  val builder      = new CopyOrderJob("orderIn", "orderOut").createTopology()
  val factoryOrder = new ConsumerRecordFactory[String, Order](new StringSerializer(), KafkaSerdes.orderSerde.serializer())

  val testDriver = new TopologyTestDriver(builder.build(), props)

  "CopyOrderJob" when {

    "is created" should {

      "start with no errors" in {
        val stream: KafkaStreams = CopyOrderJob.stream("orderIn", "orderOut")
        Thread.sleep(2000)
        stream.close()
      }
    }

    "is running" should {

      "populate output topic" in {

        val orderId  = "order_id"
        val clientId = "client_id"
        val order    = Order(orderId, clientId, 3)

        testDriver.pipeInput(factoryOrder.create("orderIn", orderId, order))

        val result = testDriver.readOutput("orderOut", new StringDeserializer, KafkaSerdes.orderSerde.deserializer())
        OutputVerifier.compareKeyValue(result, clientId, order)
      }

    }
  }
}
