package com.foo.bar.streams.flow

import akka.Done
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage
import akka.kafka.scaladsl.Consumer.Control
import akka.kafka.testkit.ConsumerResultFactory
import akka.kafka.testkit.scaladsl.ConsumerControlFactory
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{ActorMaterializer, Materializer}
import com.datastax.driver.core.{BoundStatement, PreparedStatement, Session}
import com.foo.bar.model.Customer
import com.foo.bar.streams.cassandra.CassandraSink
import com.foo.bar.streams.kafka.{Consumer, RequestSource}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.{ExecutionContext, Future}

class StreamCassandraFlowSpec extends WordSpec with Matchers with StreamCassandraFlow with ScalaFutures {

  import com.foo.bar.model.JsonFormat._

  override implicit val system: ActorSystem                = ActorSystem("testSystem")
  override implicit val materializer: Materializer         = ActorMaterializer()
  override implicit val executionContext: ExecutionContext = system.dispatcher

  val topic       = "topic1"
  val partition   = 0
  val startOffset = 0L
  val groupId     = "test"
  val requestKey  = "test"

  val requestValue = "foo"

  implicit val mockSession = mock(classOf[Session])
  val mockStatement        = mock(classOf[PreparedStatement])
  val mockBindStatement    = mock(classOf[BoundStatement])

  var outputValue: Customer = Customer("empty", "empty")

  object MockedCassandraSink extends CassandraSink[Customer] {

    override val keyspace: String = "foo"

    override implicit val session: Session                                        = mockSession
    override def statementBinder: (Customer, PreparedStatement) => BoundStatement = (_, _) => mockBindStatement
    override val preparedStatement: PreparedStatement                             = mockStatement

    override def sink: Sink[Customer, Future[Done]] =
      Sink.foreach[Customer](c => outputValue = c)

  }

  object MockedKafkaConsumerSource extends Consumer {

    val element =
      ConsumerResultFactory.committableMessage(
        new ConsumerRecord[String, String](topic, partition, startOffset, requestKey, requestValue),
        ConsumerResultFactory.committableOffset(groupId, topic, partition, startOffset, "metadata")
      )

    override def consumeStream(topic: String)(implicit system: ActorSystem): Source[ConsumerMessage.CommittableMessage[String, String], Control] =
      Source.single(element).viaMat(ConsumerControlFactory.controlFlow())(Keep.right)

  }

  val source = new RequestSource[Customer] {
    override def consumeInputRecord(st: String) = Right(Customer("foo", "bar"))
  }

  "StreamCassandraFlowSpec.flow" should {

    "be Done at streamCompletion" in {

      val flow = requestFlow(topic, source, MockedCassandraSink)(MockedKafkaConsumerSource)

      whenReady(flow) { _ shouldBe Done.done() }

    }

    "produce Somes" in {

      val flow = requestFlow(topic, source, MockedCassandraSink)(MockedKafkaConsumerSource)

      whenReady(flow) { _ =>
        outputValue shouldBe Customer("foo", "bar")
      }

    }
  }

}
